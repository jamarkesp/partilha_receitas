<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    //
    protected $table = 'Favorito';
    protected $primaryKey = 'id';

    public function receita()
    {
        return $this->belongsTo(Receita::Class);
    }

    public function utilizador()
    {
        return $this->belongsTo(User::Class);
    }
}
