<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NivelDificuldade extends Model
{
    //
    protected $table = 'NivelDificuldade';
    protected $primaryKey = 'id';

    public function receitas()
    {
        return $this->hasMany(Receita::Class);
    }
}
