<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //
    protected $table = 'Comentario';
    protected $primaryKey = 'id';

    public function receita()
    {
        return $this->belongsTo(Receita::Class);
    }

    public function utilizador()
    {
        return $this->belongsTo(User::Class);
    }
}
