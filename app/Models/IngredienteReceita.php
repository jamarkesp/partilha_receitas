<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IngredienteReceita extends Model
{
    //
    protected $table = 'IngredienteReceita';
    protected $primaryKey = 'id';

    public function ingrediente()
    {
        return $this->belongsTo(Ingrediente::Class);
    }

    public function receita()
    {
        return $this->belongsTo(Receita::Class);
    }

    public function tipounidade()
    {
        return $this->belongsTo(TipoUnidade::Class);
    }
}
