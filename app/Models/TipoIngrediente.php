<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoIngrediente extends Model
{
    //
    protected $table = 'TipoIngrediente';
    protected $primaryKey = 'id';

    public function ingredientes()
    {
        return $this->hasMany(Ingrediente::Class);
    }
}
