<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    //
    protected $table = 'Ingrediente';
    protected $primaryKey = 'id';

    public function tipoingrediente()
    {
        return $this->belongsTo(TipoIngrediente::Class);
    }

    public function ingredientesreceita()
    {
        return $this->hasMany(IngredienteReceita::Class);
    }
}
