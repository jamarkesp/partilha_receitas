<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoReceita extends Model
{
    //
    protected $table = 'TipoReceita';
    protected $primaryKey = 'id';

    public function receitas()
    {
        return $this->hasMany(Receita::Class);
    }
}
