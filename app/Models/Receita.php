<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receita extends Model
{
    //
    protected $table = 'Receita';
    protected $primaryKey = 'id';

    public function classificacoes()
    {
        return $this->hasMany(ReceitaClassificacao::Class);
    }

    public function comentarios()
    {
        return $this->hasMany(Comentario::Class);
    }

    public function favoritos()
    {
        return $this->hasMany(Favorito::Class);
    }

    public function ingredientesreceita()
    {
        return $this->hasMany(IngredienteReceita::Class);
    }

    public function niveldificuldade()
    {
        return $this->belongsTo(NivelDificuldade::Class);
    }

    public function tiporeceita()
    {
        return $this->belongsTo(TipoReceita::Class);
    }

    public function utilizador()
    {
        return $this->belongsTo(User::Class);
    }
}
