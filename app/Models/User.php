<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $table = 'User';
    protected $primaryKey = 'id';

    public function comentarios()
    {
        return $this->hasMany(Comentario::Class);
    }

    public function favoritos()
    {
        return $this->hasMany(Favorito::Class);
    }

    public function receitas()
    {
        return $this->hasMany(Receita::Class);
    }

    public function classificacoes()
    {
        return $this->hasMany(ReceitaClassificacao::Class);
    }
}
