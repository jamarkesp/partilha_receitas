<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoUnidade extends Model
{
    //
    protected $table = 'TipoUnidade';
    protected $primaryKey = 'id';

    public function ingredientesreceita()
    {
        return $this->hasMany(IngredienteReceita::Class);
    }
}
