<?php

namespace App\Http\Controllers;

use App\Models\Receita;
use App\Models\NivelDificuldade;
use App\Models\Ingrediente;
use App\Models\IngredienteReceita;
use App\Models\TipoReceita;
use App\Models\TipoUnidade;
use App\Models\Favorito;
use Illuminate\Http\Request;
//use App\Http\Controllers\Auth;

class ReceitaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $niveis = NivelDificuldade::all();
        $tiposreceitas = TipoReceita::all();
        $receitas = Receita::addSelect(['dif_nome' => NivelDificuldade::select('nome')
        ->whereColumn('receita.nivel_dificuldade_id', 'niveldificuldade.id')])
->addselect(['tr_nome' => TipoReceita::select('nome')
->whereColumn('receita.tiporeceita_id', 'tiporeceita.id')])
->get();
        
        return view('receitas.receitas', compact('niveis', 'tiposreceitas', 'receitas'));
        

    }

    public function indexfavoritos()
    {
        $user = auth()->user();
        $receitas = Receita::addSelect(['dif_nome' => NivelDificuldade::select('nome')
        ->whereColumn('receita.nivel_dificuldade_id', 'niveldificuldade.id')])
->addselect(['tr_nome' => TipoReceita::select('nome')
->whereColumn('receita.tiporeceita_id', 'tiporeceita.id')])
->addSelect(['favoritoid' => Favorito::select('id')
        ->whereColumn('receita.id', 'favorito.receita_id')
        ->where('favorito.user_id', $user->id)])
                    ->get();
        


        return view('receitas.receitasfavoritas', compact('receitas'));
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $niveis = NivelDificuldade::all();
            $tiposreceitas = TipoReceita::all();
            $receita = new Receita();
            return view('receitas.receita_create', compact('niveis', 'tiposreceitas', 'receita'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, [
                'titulo' => 'required',
                'descricao' => 'required',
                'nivel_dificuldade_id' => 'required',
                'tiporeceita_id' => 'required'
            ]
        );
        $user = auth()->user();

        $receita = new Receita;
        $receita->titulo = $request->titulo;
        $receita->descricao = $request->descricao;
        $receita->nivel_dificuldade_id = $request->nivel_dificuldade_id;
        $receita->tiporeceita_id = $request->tiporeceita_id;
        $receita->user_id = $user->id;

        //--- save
        $receita->save();   

        //-----redirect
        return redirect("/receita")->with("success", "Receita Criada!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $receita = Receita::addSelect(['dif_nome' => NivelDificuldade::select('nome')
        ->whereColumn('receita.nivel_dificuldade_id', 'niveldificuldade.id')])
->addselect(['tr_nome' => TipoReceita::select('nome')
->whereColumn('receita.tiporeceita_id', 'tiporeceita.id')])
->where('receita.id', '=', $id)
->get();
        $ingredientes = IngredienteReceita::addSelect(['ingnome' => Ingrediente::select('nome')
    ->whereColumn('ingredientereceita.ingrediente_id', 'ingrediente.id')])
    ->addSelect(['unidadenome' => TipoUnidade::select('nome') ->whereColumn('ingredientereceita.tipounidade_id', 'tipounidade.id')])
    ->where('ingredientereceita.receita_id', '=', $id)
    ->get();

    //dd($receita);
      return view('receitas.receita_show', compact('receita', 'ingredientes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
            $ings = Ingrediente::all();
            $ingreceita = new IngredienteReceita;
            $niveis = NivelDificuldade::all();
            $tiposreceitas = TipoReceita::all();
            $tunidades = TipoUnidade::all();
            $ingredientes = IngredienteReceita::addSelect(['ingnome' => Ingrediente::select('nome')
        ->whereColumn('ingredientereceita.ingrediente_id', 'ingrediente.id')])
        ->addSelect(['unidadenome' => TipoUnidade::select('nome') ->whereColumn('ingredientereceita.tipounidade_id', 'tipounidade.id')])
        ->where('ingredientereceita.receita_id', '=', $id)
        ->get();
            $receita = Receita::find($id);
            return view('receitas.receita_details', compact('ings', 'ingreceita',  'niveis', 'tiposreceitas', 'tunidades', 'ingredientes',  'receita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request, [
                'titulo' => 'required',
                'descricao' => 'required',
                'nivel_dificuldade_id' => 'required',
                'tiporeceita_id' => 'required'
            ]
        );

        $receita = Receita::find($id);
        $receita->titulo = $request->titulo;
        $receita->descricao = $request->descricao;
        $receita->nivel_dificuldade_id = $request->nivel_dificuldade_id;
        $receita->tiporeceita_id = $request->tiporeceita_id;

        $receita->save();

        //-----redirect
        return redirect("/receita")->with("success", "Receita Atualizada!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Receita  $receita
     * @return \Illuminate\Http\Response
     */
    public function destroy(Receita $receita)
    {
        //
    }
}
