<?php

namespace App\Http\Controllers;

use App\Models\TipoUnidade;
use Illuminate\Http\Request;

class TipoUnidadeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tipounidades.tipounidade', ['tunidades' => TipoUnidade::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoUnidade = new TipoUnidade();
        return view('tipounidades.tipounidade_create',['tunidade' => $tipoUnidade]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $this->validate(
            $request, [
                'nome' => 'required',
                'sigla' => 'required'
            ]
        );


        $tipoUnidade = new TipoUnidade;
        $tipoUnidade->nome = $request->nome;
        $tipoUnidade->sigla = $request->sigla;

        //--- save
        $tipoUnidade->save();   

        //-----redirect
        return redirect("/tipounidade")->with("success", "Tipo Unidade Criado!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoUnidade  $tipoUnidade
     * @return \Illuminate\Http\Response
     */
    public function show(TipoUnidade $tipoUnidade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoUnidade  $tipoUnidade
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoUnidade = TipoUnidade::find($id);
        return view('tipounidades.tipounidade_details',['tunidade' => $tipoUnidade]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoUnidade  $tipoUnidade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request);

        $this->validate(
            $request, [
                'nome' => 'required',
                'sigla' => 'required'
            ]
        );

        
        $tipoUnidade = TipoUnidade::find($id);
        $tipoUnidade->nome = $request->nome;
        $tipoUnidade->sigla = $request->sigla;
        //dd($tipoUnidade);
        //--- save
        $tipoUnidade->save();

        //-----redirect
        return redirect("/tipounidade")->with("success", "Tipo Unidade atualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoUnidade  $tipoUnidade
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoUnidade $tipoUnidade)
    {
        //
    }
}
