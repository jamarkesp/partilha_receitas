<?php

namespace App\Http\Controllers;

use App\Models\Ingrediente;
use App\Models\TipoIngrediente;
use Illuminate\Http\Request;

class IngredienteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ingredientes.ingredientes', ['ings' => Ingrediente::addSelect(['ting_nome' => TipoIngrediente::select('nome')
                                                                                    ->whereColumn('ingrediente.tipoingrediente_id', 'tipoingrediente.id')])
                                                    ->get()]);
        


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = TipoIngrediente::all();
        $ingrediente = new Ingrediente();
        return view('ingredientes.ingrediente_create', compact('tipos', 'ingrediente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, [
                'nome' => 'required',
                'tipoingrediente_id' => 'required'
            ]
        );


        $ingrediente = new Ingrediente;
        $ingrediente->nome = $request->nome;
        $ingrediente->tipoingrediente_id = $request->tipoingrediente_id;

        //--- save
        $ingrediente->save();   

        //-----redirect
        return redirect("/ingrediente")->with("success", "Ingrediente Criado!");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ingrediente  $ingrediente
     * @return \Illuminate\Http\Response
     */
    public function show(Ingrediente $ingrediente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ingrediente  $ingrediente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipos = TipoIngrediente::all();
        $ingrediente = Ingrediente::find($id);
        return view('ingredientes.ingrediente_details', compact('tipos', 'ingrediente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ingrediente  $ingrediente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request, [
                'nome' => 'required',
                'tipoingrediente_id' => 'required'
            ]
        );

        $ingrediente = Ingrediente::find($id);
        $ingrediente->nome = $request->nome;
        $ingrediente->tipoingrediente_id = $request->tipoingrediente_id;

        $ingrediente->save();

        //-----redirect
        return redirect("/ingrediente")->with("success", "Ingrediente Atualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ingrediente  $ingrediente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ingrediente $ingrediente)
    {
        //
    }
}
