<?php

namespace App\Http\Controllers;

use App\Models\NivelDificuldade;
use Illuminate\Http\Request;

class NivelDificuldadeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('niveisdificuldade.niveisdificuldade', ['ndifs' => NivelDificuldade::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nivelDificuldade = new NivelDificuldade();
        return view('niveisdificuldade.niveldificuldade_create',['ndif' => $nivelDificuldade]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $this->validate(
            $request, [
                'nome' => 'required'
            ]
        );


        $nivelDificuldade = new NivelDificuldade;
        $nivelDificuldade->nome = $request->nome;

        //--- save
        $nivelDificuldade->save();   

        //-----redirect
        return redirect("/niveldificuldade")->with("success", "Nível de Dificuldade Criado!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NivelDificuldade  $nivelDificuldade
     * @return \Illuminate\Http\Response
     */
    public function show(NivelDificuldade $nivelDificuldade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NivelDificuldade  $nivelDificuldade
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nivelDificuldade = NivelDificuldade::find($id);
        return view('niveisdificuldade.niveldificuldade_details',['ndif' => $nivelDificuldade]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NivelDificuldade  $nivelDificuldade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request);

        $this->validate(
            $request, [
                'nome' => 'required'
            ]
        );

        
        $nivelDificuldade = NivelDificuldade::find($id);
        $nivelDificuldade->nome = $request->nome;
        //dd($tipoReceita);
        //--- save
        $nivelDificuldade->save();

        //-----redirect
        return redirect("/niveldificuldade")->with("success", "Nível de Dificuldade atualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NivelDificuldade  $nivelDificuldade
     * @return \Illuminate\Http\Response
     */
    public function destroy(NivelDificuldade $nivelDificuldade)
    {
        //
    }
}
