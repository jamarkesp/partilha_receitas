<?php

namespace App\Http\Controllers;

use App\Models\TipoReceita;
use Illuminate\Http\Request;

class TipoReceitaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tiporeceitas.tiporeceitas', ['treceitas' => TipoReceita::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoReceita = new TipoReceita();
        return view('tiporeceitas.tiporeceita_create',['treceita' => $tipoReceita]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);

        $this->validate(
            $request, [
                'nome' => 'required'
            ]
        );


        $tipoReceita = new TipoReceita;
        $tipoReceita->nome = $request->nome;

        //--- save
        $tipoReceita->save();   

        //-----redirect
        return redirect("/tiporeceita")->with("success", "Tipo de Receita Criado!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TipoReceita  $tipoReceita
     * @return \Illuminate\Http\Response
     */
    public function show(TipoReceita $tipoReceita)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TipoReceita  $tipoReceita
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoReceita = TipoReceita::find($id);
        return view('tiporeceitas.tiporeceita_details',['treceita' => $tipoReceita]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TipoReceita  $tipoReceita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //dd($request);

        $this->validate(
            $request, [
                'nome' => 'required'
            ]
        );

        
        $tipoReceita = TipoReceita::find($id);
        $tipoReceita->nome = $request->nome;
        //dd($tipoReceita);
        //--- save
        $tipoReceita->save();

        //-----redirect
        return redirect("/tiporeceita")->with("success", "Tipo de Receita atualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TipoReceita  $tipoReceita
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoReceita $tipoReceita)
    {
        //
    }
}
