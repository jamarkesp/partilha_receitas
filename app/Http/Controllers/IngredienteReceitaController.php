<?php

namespace App\Http\Controllers;

use App\Models\IngredienteReceita;
use Illuminate\Http\Request;

class IngredienteReceitaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $receitaid)
    {
        $this->validate(
            $request, [
                'ingrediente_id' => 'required',
                'quantidade' => 'required',
                'tipounidade_id' => 'required'
            ]
        );
        $ingreceita = new IngredienteReceita;
        $ingreceita->receita_id = $receitaid;
        $ingreceita->ingrediente_id = $request->ingrediente_id;
        $ingreceita->quantidade = $request->quantidade;
        $ingreceita->tipounidade_id = $request->tipounidade_id;

        $ingreceita->save();

        return redirect()->route('receita.update', ['Receita' => $receitaid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\IngredienteReceita  $ingredienteReceita
     * @return \Illuminate\Http\Response
     */
    public function show(IngredienteReceita $ingredienteReceita)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\IngredienteReceita  $ingredienteReceita
     * @return \Illuminate\Http\Response
     */
    public function edit(IngredienteReceita $ingredienteReceita)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\IngredienteReceita  $ingredienteReceita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IngredienteReceita $ingredienteReceita)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IngredienteReceita  $ingredienteReceita
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ingreceita = IngredienteReceita::find($id);
        $receitaid = $ingreceita->receita_id;
        $ingreceita ->delete();

        return redirect()->route('receita.update', ['Receita' => $receitaid]);
        
    }
}
