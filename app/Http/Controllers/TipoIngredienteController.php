<?php

namespace App\Http\Controllers;

use App\Models\TipoIngrediente;
use Illuminate\Http\Request;

class TipoIngredienteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tipoingredientes.tipoingredientes', ['tings' => TipoIngrediente::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoIngrediente = new TipoIngrediente();
        return view('tipoingredientes.tipoingredientes_create',['tings' => $tipoIngrediente]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request);

         $this->validate(
            $request, [
                'nome' => 'required'
            ]
        );


        $tipoIngrediente = new TipoIngrediente;
        $tipoIngrediente->nome = $request->nome;

        //--- save
        $tipoIngrediente->save();   

        //-----redirect
        return redirect("/tipoingrediente")->with("success", "Tipo de ingrediente criado!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TipoIngrediente  $tipoIngrediente
     * @return \Illuminate\Http\Response
     */
    public function show(TipoIngrediente $tipoIngrediente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TipoIngrediente  $tipoIngrediente
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoIngrediente = TipoIngrediente::find($id);
        return view('tipoingredientes.tipoingredientes_details',['tings' => $tipoIngrediente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TipoIngrediente  $tipoIngrediente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(
            $request, [
                'nome' => 'required'
            ]
        );

        
        $tipoIngrediente = TipoIngrediente::find($id);
        $tipoIngrediente->nome = $request->nome;
      
        //--- save
        $tipoIngrediente->save();

        //-----redirect
        return redirect("/tipoingrediente")->with("success", "Tipo de ingrediente atualizado!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TipoIngrediente  $tipoIngrediente
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoIngrediente $tipoIngrediente)
    {
        //
    }
}
