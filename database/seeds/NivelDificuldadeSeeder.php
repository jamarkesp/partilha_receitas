<?php

use Illuminate\Database\Seeder;

class NivelDificuldadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DB::table('NivelDificuldade')->count();
        if($count == 0){
            DB::table('NivelDificuldade')->insert([
                [
                    'nome' => 'Alto',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'nome' => 'Médio',
                    'created_at' => now(),
                    'updated_at' => now(),
                ],
                [
                    'nome' => 'Baixo',
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            ]);
        }
    }
}
