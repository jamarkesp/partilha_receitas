<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DB::table('roles')->count();
        if($count == 0){
            DB::table('roles')->insert([
                'name' => 'Admin',
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
