<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = DB::table('users')->count();
        if($count == 0){
            DB::table('users')->insert([
                [
                    'name' => 'Administrador',
                    'email' => 'administrador@xpto.com',
                    'email_verified_at' => now(),
                    'password' => Hash::make('administrador'),
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            ]);
}

    }
}
