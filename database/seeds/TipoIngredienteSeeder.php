<?php

use Illuminate\Database\Seeder;

class TipoIngredienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DB::table('TipoIngrediente')->count();
        if($count == 0){
            DB::table('TipoIngrediente')->insert([
                [
                    'nome' => 'Legumes',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'nome' => 'Especiarias',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'nome' => 'Fruta',
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]);
        }
    }
}
