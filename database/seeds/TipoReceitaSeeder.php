<?php

use Illuminate\Database\Seeder;

class TipoReceitaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = DB::table('users')->count();
        if($count == 0){
            DB::table('users')->insert([
                [
                    'nome' => 'Entradas',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'nome' => 'Almoços',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'nome' => 'Sobremesas',
                    'created_at' => now(),
                    'updated_at' => now()
                ]
            ]);
        }
    }
}
