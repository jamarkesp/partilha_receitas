<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IngredienteReceitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IngredienteReceita', function (Blueprint $table) {
            $table->id();
            $table->foreignId('receita_id')->constrained('Receita')->onDelete('cascade');
            $table->foreignId('ingrediente_id')->constrained('Ingrediente')->onDelete('cascade');
            $table->string('quantidade');
            $table->foreignId('tipounidade_id')->constrained('TipoUnidade')->onDelete('cascade');    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('IngredienteReceita');
    }
}
