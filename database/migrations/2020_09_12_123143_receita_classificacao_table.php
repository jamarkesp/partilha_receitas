<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReceitaClassificacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ReceitaClassificacao', function (Blueprint $table) {
            $table->id();
            $table->foreignId('receita_id')->constrained('Receita')->onDelete('cascade');
            $table->string('classificacao');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');  
            $table->string('comentario');  
            $table->timestamps();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ReceitaClassificacao');
    }
}
