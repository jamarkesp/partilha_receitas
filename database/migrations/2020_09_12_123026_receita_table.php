<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReceitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Receita', function (Blueprint $table) {
            $table->id();
            $table->string('titulo')->nullable(false)->unique();
            $table->string('descricao');
            $table->foreignId('nivel_dificuldade_id')->constrained('NivelDificuldade')->onDelete('cascade');
            $table->foreignId('tiporeceita_id')->constrained('TipoReceita')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');       
            $table->timestamps();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Receita');
    }
}
