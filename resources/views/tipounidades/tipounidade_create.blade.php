@extends('layouts.app', [
    'namePage' => 'Dados de Refêrencia',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'tipounidade',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="title">{{__(" Novo Tipo Unidade")}}</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('tipounidade.create') }}" autocomplete="off" enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('alerts.success')
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                            <label>{{__(" Nome")}}</label>
                            <input type="text" name="nome" class="form-control" value="{{ old('nome', $tunidade->nome) }}" placeholder="Tipo Unidade Nome">
                            @include('alerts.feedback', ['field' => 'nome'])
                        </div>
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                            <label>{{__(" Sigla")}}</label>
                            <input type="text" name="sigla" class="form-control" value="{{ old('sigla', $tunidade->sigla) }}" placeholder="Tipo Unidade Sigla">
                            @include('alerts.feedback', ['field' => 'sigla'])
                        </div>
                    </div> 
                </div> 

                <div class="card-footer"> 
                    <div class="form-edit-meta">
                        <button type="submit" class="btn btn-primary btn-round">{{__('Guardar')}}</button>
                        <a class="btn btn-round" href="/tipounidade">Cancelar</a>
                    </div>
                </div>

            </form>

                



        </div>
    </div>
  </div>
  </div>
</div>


@endsection