<div class="sidebar" data-color="orange">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
-->
<div class="logo">
  <a href="#" class="simple-text logo-mini">
    {{ __('') }}
  </a>
  <a href="/home" class="simple-text logo-normal">
    {{ __('Receitas App') }}
  </a>
</div>

  <div class="sidebar-wrapper" id="sidebar-wrapper">
    <ul class="nav">
      
      <li>
        <a data-toggle="collapse" href="#RefData">
            <i class="now-ui-icons  text_align-left"></i>
          <p>
            {{ __("Dados de referencia") }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse show" id="RefData">
          <ul class="nav">
            <li class="@if ($activePage == 'tipounidade') active @endif">
              <a href="{{ route('tipounidade') }}">
                <i class="now-ui-icons design_bullet-list-67"></i>
                <p> {{ __("Tipos de unidade") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'tiporeceita') active @endif">
              <a href="{{ route('tiporeceita') }}">
                <i class="now-ui-icons design_bullet-list-67"></i>
                <p> {{ __("Tipos de Receita") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'niveldificuldade') active @endif">
              <a href="{{ route('niveldificuldade') }}">
                <i class="now-ui-icons design_bullet-list-67"></i>
                <p> {{ __("Níveis de Dificuldade") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'tipoingrediente') active @endif">
              <a href="{{ route('tipoingrediente') }}">
                <i class="now-ui-icons design_bullet-list-67"></i>
                <p> {{ __("Tipos de Ingrediente") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'ingrediente') active @endif">
              <a href="{{ route('ingrediente') }}">
                <i class="now-ui-icons design_bullet-list-67"></i>
                <p> {{ __("Ingredientes") }} </p>
              </a>
            </li>
            <li class="@if ($activePage == 'utilizadores') active @endif">
              <a href="{{ route('user.index') }}">
                <i class="now-ui-icons design_bullet-list-67"></i>
                <p> {{ __("Gestão de utilizadores") }} </p>
              </a>
            </li>
          </ul>
        </div>

        <li class="@if ($activePage == 'receita') active @endif">
        <a href="{{ route('receita') }}">
          <i class="now-ui-icons ui-2_favourite-28"></i>
          <p>{{ __('Lista de Receitas') }}</p>
        </a>
      </li>   

    </ul>
  </div>
</div>