<footer class="footer">
  <div class=" container-fluid ">
  </div>
  <div class=" container-fluid ">
    
    <div class="copyright" id="copyright">
      &copy;
      <script>
        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
      </script>, {{__(" Designed by")}}
      <a href="#" target="_blank">{{__(" Invision")}}</a>{{__(" . Coded by")}}
      <a href="#" target="_blank">{{__(" Daniela ")}}</a>&
      <a href="#" target="_blank">{{__(" José ")}}</a>&
      <a href="#" target="_blank">{{__(" Vasco")}}</a>
    </div>
  </div>
</footer>