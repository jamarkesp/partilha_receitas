@extends('layouts.app', [
    'namePage' => 'Receita',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'receita',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="title">{{__("Editar Receita")}}</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('receita.update',[$receita->id]) }}" autocomplete="off" enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('alerts.success')
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                            <label>{{__(" Título")}}</label>
                            <input type="text" name="titulo" class="form-control" value="{{ old('titulo', $receita->titulo) }}" placeholder="Título da Receita">
                            @include('alerts.feedback', ['field' => 'titulo'])
                        </div>
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                            <label>{{__(" Descrição")}}</label>
                            <input type="text" name="descricao" class="form-control" value="{{ old('descricao', $receita->descricao) }}" placeholder="Descrição da Receita">
                            @include('alerts.feedback', ['field' => 'descricao'])
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                    <label for="nivel_dificuldade_id">Nivel de Dificuldade</label>
                    <select name="nivel_dificuldade_id" id="nivel_dificuldade_id" class="form-control select2">
                        @foreach ($niveis as $nivel)
                            <option value="{{ $nivel->id }}" {{ $nivel->id == $nivel->nivel_dificuldade_id ? 'selected' : '' }}>{{ $nivel->nome }}</option>
                        @endforeach
                    </select>
                    @include('alerts.feedback', ['field' => 'nivel_dificuldade_id'])
                </div>
                </div> 
                  </div>        
                  <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                    <label for="tiporeceita_id">Tipo de Receita</label>
                    <select name="tiporeceita_id" id="tiporeceita_id" class="form-control select2">
                        @foreach ($tiposreceitas as $tipo)
                            <option value="{{ $tipo->id }}" {{ $tipo->id == $tipo->tiporeceita_id ? 'selected' : '' }}>{{ $tipo->nome }}</option>
                        @endforeach
                    </select>
                    @include('alerts.feedback', ['field' => 'tiporeceita_id'])
                </div>
                </div>
                  </div>

                <div class="card-footer"> 
                    <div class="form-edit-meta">
                        <button type="submit" class="btn btn-primary btn-round">{{__('Guardar')}}</button>
                        <a class="btn btn-round" href="/receita">Cancelar</a>
                    </div>
                </div>

            </form>





        </div>
    </div>
    @if ($receita->id == 0)
                 
    @else
    <div class="card">
        <div class="card-header">
          <h5 class="title">{{__("Associar Ingredientes")}}</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('ingreceita.create',[$receita->id]) }}" autocomplete="off" enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('alerts.success')
                <div class="row">
                    <div class="col-md-10 pr-1">
                        <div class="form-group">
                            <label for="ingrediente_id">Ingrediente</label>
                            <select name="ingrediente_id" id="ingrediente_id" class="form-control select2">
                            @foreach ($ings as $ing)
                                <option value="{{ $ing->id }}" {{ $ing->id == $ingreceita->ingrediente_id ? 'selected' : '' }}>{{ $ing->nome }}</option>
                            @endforeach
                        </select>
                        </div>


                        <div class="form-group">
                            <label>{{__("Quantidade")}}</label>
                            <input type="number" name="quantidade" class="form-control" value="{{ old('quantidade', $ingreceita->quantidade) }}" placeholder="Quantidade do Ingrediente">
                            @include('alerts.feedback', ['field' => 'quantidade'])
                        </div>

                        <div class="form-group">
                            <label for="tipounidade_id">Tipo de Unidade</label>
                            <select name="tipounidade_id" id="tipounidade_id" class="form-control select2">
                            @foreach ($tunidades as $tunidade)
                                <option value="{{ $tunidade->id }}" {{ $tunidade->id == $ingreceita->tipounidade_id ? 'selected' : '' }}>{{ $tunidade->nome }}</option>
                            @endforeach
                        </select>
                        </div>
                </div>
                <div class="col-md-2" align="right">
                    <button type="submit" class="btn btn-primary btn-round">{{__('Guardar')}}</button>                        
                  </div>
            </form>

            <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Nome</th>
                    <th>Quantidade</th>
                  </thead>
                  <tbody>               
                    @foreach ($ingredientes as $ingrediente)
                        <tr>
                            <td>{{$ingrediente->ingnome}}</td>
                            <td>{{$ingrediente->quantidade}} {{$ingrediente->unidadenome}}</td>
                            <td>
                              <a href="/ingreceita/remove/{{$ingrediente->id}}">
                                <i class="fas fa-trash" style="font-size:1.2rem" title="Eliminar"></i>
                              </a>
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>  
        </div>
    </div>
    @endif
  </div>
  </div>
</div>


@endsection