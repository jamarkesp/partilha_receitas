@extends('layouts.app', [
    'namePage' => 'Receitas',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'receita',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])


@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header row">
          <div class="col-md-10">
            <h4 class="card-title">Receitas</h4>
          </div>
          <div class="col-md-2 card-title" align="right">
            <a href="/receita/new">
              <i class="fa fa-plus" style="font-size:1.2rem" title="Adicionar"></i>
            </a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>Título</th>
                <th>Descrição</th>
                <th>Tipo de Receita</th>
                <th>Nível de Dificuldade</th>
                <th>Criado Em</th>
                <th>Atualizado Em</th>
              </thead>
              <tbody>               
                @foreach ($receitas as $receita)
                    <tr>
                        <td>{{$receita->titulo}}</td>
                        <td>{{$receita->descricao}}</td>
                        <td>{{$receita->tr_nome}}</td>
                        <td>{{$receita->dif_nome}}</td>
                        <td>{{$receita->created_at}}</td>
                        <td>{{$receita->updated_at}}</td>
                        <td>
                          <a href="/receita/edit/{{$receita->id}}">
                            <i class="fa fa-edit" style="font-size:1.2rem" title="Editar"></i>
                          </a>
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection