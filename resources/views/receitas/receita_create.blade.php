@extends('layouts.app', [
    'namePage' => 'Receita',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'receita',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="title">{{__(" Nova Receita")}}</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('receita.create') }}" autocomplete="off" enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('alerts.success')
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                            <label>{{__(" Título")}}</label>
                            <input type="text" name="titulo" class="form-control" value="{{ old('titulo', $receita->titulo) }}" placeholder="Título da Receita">
                            @include('alerts.feedback', ['field' => 'titulo'])
                        </div>
                    </div> 
                </div> 
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                            <label>{{__(" Descrição")}}</label>
                            <input type="text" name="descricao" class="form-control" value="{{ old('descricao', $receita->descricao) }}" placeholder="Descrição da Receita">
                            @include('alerts.feedback', ['field' => 'descricao'])
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-7 pr-1">
                    <label for="nivel_dificuldade_id">Nivel de Dificuldade</label>
                    <select name="nivel_dificuldade_id" id="nivel_dificuldade_id" class="form-control select2">
                        @foreach ($niveis as $nivel)
                            <option value="{{ $nivel->id }}" {{ $nivel->id == $nivel->nivel_dificuldade_id ? 'selected' : '' }}>{{ $nivel->nome }}</option>
                        @endforeach
                    </select>
                </div> 
                  </div>        
                  <div class="row">
                    <div class="col-md-7 pr-1">
                    <label for="tiporeceita_id">Tipo de Receita</label>
                    <select name="tiporeceita_id" id="tiporeceita_id" class="form-control select2">
                        @foreach ($tiposreceitas as $tipo)
                            <option value="{{ $tipo->id }}" {{ $tipo->id == $tipo->tiporeceita_id ? 'selected' : '' }}>{{ $tipo->nome }}</option>
                        @endforeach
                    </select>
                </div>
                  </div>

                <div class="card-footer"> 
                    <div class="form-edit-meta">
                        <button type="submit" class="btn btn-primary btn-round">{{__('Guardar')}}</button>
                        <a class="btn btn-round" href="/receita">Cancelar</a>
                    </div>
                </div>

            </form>

                



        </div>
    </div>
  </div>
  </div>
</div>


@endsection