@extends('layouts.app', [
    'namePage' => 'Receita',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'receita',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="title">{{__("Visualização da Receita")}}</h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-7 pr-1">
                    <div class="form-group">
                        <label>{{__(" Título")}}</label>
                        <div>{{$receita[0]->titulo }}</div>
                    </div>
                </div> 
            </div> 
            <div class="row">
                <div class="col-md-7 pr-1">
                    <div class="form-group">
                        <label>{{__(" Descrição")}}</label>
                        <div>{{$receita[0]->descricao }}</div>
                    </div>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-7 pr-1">
                    <div class="form-group">
                        <label>{{__("Nível de Dificuldade")}}</label>
                        <div>{{$receita[0]->dif_nome }}</div>
                    </div>
                </div> 
            </div>        
            <div class="row">
                <div class="col-md-7 pr-1">
                    <div class="form-group">
                        <label>{{__("Tipo de Receita")}}</label>
                        <div>{{$receita[0]->tr_nome }}</div>
                    </div>
                </div> 
            </div>
    </div>
</div>
    <div class="card">
        <div class="card-header">
          <h5 class="title">{{__("Ingredientes Associados")}}</h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Nome</th>
                    <th>Quantidade</th>
                  </thead>
                  <tbody>               
                    @foreach ($ingredientes as $ingrediente)
                        <tr>
                            <td>{{$ingrediente->ingnome}}</td>
                            <td>{{$ingrediente->quantidade}} {{$ingrediente->unidadenome}}</td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>  
        </div>
    </div>
  </div>
  </div>
</div>


@endsection