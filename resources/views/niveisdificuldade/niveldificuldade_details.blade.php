@extends('layouts.app', [
    'namePage' => 'Dados de Refêrencia',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'niveldificuldade',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="title">{{__("Editar Nível de Dificuldade")}}</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('niveldificuldade.update',[$ndif->id]) }}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                @method('put')
                @include('alerts.success')
                    <div class="row">
                        <div class="col-md-7 pr-1">
                            <div class="form-group">
                                <label>{{__("Nome")}}</label>
                                <input type="text" name="nome" class="form-control" value="{{ old('nome', $ndif->nome) }}" placeholder="Nível de Dificuldade Nome">
                                @include('alerts.feedback', ['field' => 'nome'])
                            </div>
                        </div> 
                    </div> 
                    <div class="card-footer"> 
                        <div class="form-edit-meta">
                            <button type="submit" class="btn btn-primary btn-round">{{__('Guardar')}}</button>
                            <a class="btn btn-round" href="/niveldificuldade">Cancelar</a>
                        </div>
                    </div>    
                </form>
        </div>
    </div>
  </div>
  </div>
</div>
@endsection