@extends('layouts.app', [
    'namePage' => 'Dados de Refêrencia',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'utilizadores',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Utilizadores</h4>
          <div class="col-12 mt-2">
                                      </div>
        </div>
        <div class="card-body">
          <div class="toolbar">
            <!--        Here you can write extra buttons/actions for the toolbar              -->
          </div>
          <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Email</th>
                <th>Criado Em</th>
                <th>Atualizado Em</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($users as $user)
              <tr>
                  <td>
                      {{$user->name}}
                  </td>
                  <td>
                    {{$user->email}}
                  </td>
                  <td>
                      {{$user->created_at}}
                  </td>
                  <td>
                    {{$user->updated_at}}
                  </td>
              </tr>
          @endforeach
                            </tbody>
          </table>
        </div>
@endsection