@extends('layouts.app', [
    'namePage' => 'Dados de Refêrencia',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'tiporeceita',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="title">{{__(" Novo Tipo de Receita")}}</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('tiporeceita.create') }}" autocomplete="off" enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('alerts.success')
                <div class="row">
                    <div class="col-md-7 pr-1">
                        <div class="form-group">
                            <label>{{__("Nome")}}</label>
                            <input type="text" name="nome" class="form-control" value="{{ old('nome', $treceita->nome) }}" placeholder="Tipo Receita Nome">
                            @include('alerts.feedback', ['field' => 'nome'])
                        </div>
                    </div> 
                </div> 
                <div class="card-footer"> 
                    <div class="form-edit-meta">
                        <button type="submit" class="btn btn-primary btn-round">{{__('Guardar')}}</button>
                        <a class="btn btn-round" href="/tiporeceita">Cancelar</a>
                    </div>
                </div>

            </form>

                



        </div>
    </div>
  </div>
  </div>
</div>


@endsection