@extends('layouts.app', [
    'namePage' => 'Dados de Refêrencia',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'ingrediente',
    'backgroundImage' => asset('now') . "/img/bg14.jpg",
])

@section('content')
<div class="panel-header panel-header-sm">
</div>
<div class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header row">
          <div class="col-md-10">
            <h4 class="card-title">Ingredientes</h4>
          </div>
          <div class="col-md-2 card-title" align="right">
            <a href="/ingrediente/new">
              <i class="fa fa-plus" style="font-size:1.2rem" title="Adicionar"></i>
            </a>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>
                  Nome
                </th>
                <th>
                  Tipo de Ingrediente
                </th>
                <th>
                  Criado Em
                </th>
                <th>
                  Atualizado Em
                </th>
              </thead>
              <tbody>                
                @foreach ($ings as $ing)
                    <tr>
                        <td>
                          {{$ing->nome}}
                        </td>
                        <td>
                          {{$ing->ting_nome}}
                        </td>
                        <td>
                          {{$ing->created_at}}
                      </td>
                      <td>
                        {{$ing->updated_at}}
                    </td>
                    <td>
                      <a href="/ingrediente/edit/{{$ing->id}}">
                        <i class="fa fa-edit" style="font-size:1.2rem" title="Editar"></i>
                      </a>
                    </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

@endsection