<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'receita', 'uses' => 'ReceitaController@index']);
Route::get('/home', ['as' => 'receita', 'uses' => 'ReceitaController@index']);

Auth::routes();

Route::prefix('tipounidade')->group(function() {

	Route::get('/edit/{tipoUnidade}', 'TipoUnidadeController@edit');
	Route::put('/edit/{tipoUnidade}', ['as' => 'tipounidade.update', 'uses' => 'TipoUnidadeController@update']);

	Route::get('/new', 'TipoUnidadeController@create');
	Route::put('/new', ['as' => 'tipounidade.create', 'uses' => 'TipoUnidadeController@store']);

	Route::get('/', ['as' => 'tipounidade', 'uses' => 'TipoUnidadeController@index']);

});

Route::prefix('tiporeceita')->group(function() {

	Route::get('/edit/{tipoReceita}', 'TipoReceitaController@edit');
	Route::put('/edit/{tipoReceita}', ['as' => 'tiporeceita.update', 'uses' => 'TipoReceitaController@update']);

	Route::get('/new', 'TipoReceitaController@create');
	Route::put('/new', ['as' => 'tiporeceita.create', 'uses' => 'TipoReceitaController@store']);

	Route::get('/', ['as' => 'tiporeceita', 'uses' => 'TipoReceitaController@index']);

});

Route::prefix('tipoingrediente')->group(function() {

	Route::get('/edit/{tipoIngrediente}', 'TipoIngredienteController@edit');
	Route::put('/edit/{tipoIngrediente}', ['as' => 'tipoingrediente.update', 'uses' => 'TipoIngredienteController@update']);

	Route::get('/new', 'TipoIngredienteController@create');
	Route::put('/new', ['as' => 'tipoingrediente.create', 'uses' => 'TipoIngredienteController@store']);

	Route::get('/', ['as' => 'tipoingrediente', 'uses' => 'TipoIngredienteController@index']);

});


Route::prefix('niveldificuldade')->group(function() {

	Route::get('/edit/{nivelDificuldade}', 'NivelDificuldadeController@edit');
	Route::put('/edit/{nivelDificuldade}', ['as' => 'niveldificuldade.update', 'uses' => 'NivelDificuldadeController@update']);

	Route::get('/new', 'NivelDificuldadeController@create');
	Route::put('/new', ['as' => 'niveldificuldade.create', 'uses' => 'NivelDificuldadeController@store']);

	Route::get('/', ['as' => 'niveldificuldade', 'uses' => 'NivelDificuldadeController@index']);

});

Route::prefix('ingrediente')->group(function() {

	Route::get('/edit/{Ingrediente}', 'IngredienteController@edit');
	Route::put('/edit/{Ingrediente}', ['as' => 'ingrediente.update', 'uses' => 'IngredienteController@update']);

	Route::get('/new', 'IngredienteController@create');
	Route::put('/new', ['as' => 'ingrediente.create', 'uses' => 'IngredienteController@store']);

	Route::get('/', ['as' => 'ingrediente', 'uses' => 'IngredienteController@index']);

});

Route::prefix('receita')->group(function() {

	Route::get('/edit/{Receita}', 'ReceitaController@edit');
	Route::put('/edit/{Receita}', ['as' => 'receita.update', 'uses' => 'ReceitaController@update']);

	Route::get('/new', 'ReceitaController@create');
	Route::put('/new', ['as' => 'receita.create', 'uses' => 'ReceitaController@store']);

	Route::get('/show/{Receita}', 'ReceitaController@show');

	Route::get('/favoritos', ['as' => 'receita.favoritos', 'uses' => 'ReceitaController@indexfavoritos']);

	Route::get('/', ['as' => 'receita', 'uses' => 'ReceitaController@index']);

});

Route::prefix('ingreceita')->group(function() {

	Route::get('/edit/{IngReceita}', 'IngredienteReceitaController@edit');
	Route::put('/edit/{IngReceita}', ['as' => 'ingreceita.update', 'uses' => 'IngredienteReceitaController@update']);

	Route::get('/new', 'IngredienteReceitaController@create');
	Route::put('/new/{receitaid}', ['as' => 'ingreceita.create', 'uses' => 'IngredienteReceitaController@store']);

	Route::get('/remove/{IngReceita}', 'IngredienteReceitaController@destroy');

	Route::get('/', ['as' => 'ingreceita', 'uses' => 'IngredienteReceitaController@index']);


});






Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);
});

